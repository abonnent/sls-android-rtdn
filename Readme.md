# sls-android-rtdn
## Setup
_Note_: This assumes that you already have created an organisation in GCP and that set up a billing account and all that's necessary. It also assumes you have the [Google cloud SDK installed](https://cloud.google.com/sdk/install) and configured on your machine.
### Create and configure the project:
1. Create the project
    ```
    $ gcloud projects create sls-android-rtdn --organization YOUR_ORGANISATION_ID --set-as-default
    ```
2. The serverless framework needs additional APIs enabled within GCP to work. The tutorial states that the following ones should be enabled, however most of them are already enabled. But the easy way to enable them is to run the following command:
    ```
    gcloud services enable cloudfunctions.googleapis.com deploymentmanager.googleapis.com storage-api.googleapis.com logging.googleapis.com
    ```
    If you run into an error like `ERROR: (gcloud.services.enable) FAILED_PRECONDITION: Operation does not satisfy the following requirements: billing-enabled {Billing must be enabled for activation of service '' in project 'sls-android-rtdn' to proceed.` you haven't configured your project properly and it can't be billed. This means that your project is not enabled for billing. Most likely this means that you need to log into the `sysadmin` account and link either via the console (Check _Billing_ in the burger menu.) or do it via the `gcloud` sdk after you authenticated:
    1. Get the account id of the billing account via:
        ```
        gcloud beta billing accounts list
        ```
    2. Link the new project with the extracted account id:
        ```
        gcloud beta billing projects link sls-android-rtdn --billing-account BILLING_ACCOUNT_ID
        ```

### Create the service account used by the Serverless framework
_Note_: You can either follow [this](https://serverless.com/framework/docs/providers/google/guide/credentials/) tutorial or use the gcloud sdk.

1. Create a service account for your user that is going to be used by the sls framework:
    ```
    gcloud iam service-accounts create sls-your-name --display-name "Serverless Service Account YOUR NAME"
    ```
2. Add the necessary roles to the created service account:
    ```
    gcloud projects add-iam-policy-binding sls-android-rtdn --member serviceAccount:sls-your-name@sls-android-rtdn.iam.gserviceaccount.com --role roles/deploymentmanager.editor --role roles/storage.admin --role roles/logging.admin --role roles/cloudfunctions.developer --role roles/editor
    ```
3. Create keys for the created service account:
    ```
    gcloud iam service-accounts keys create --iam-account sls-your-name@sls-android-rtdn.iam.gserviceaccount.com CREDENTIALS.json
    ```
4. Move the generated `CREDENTIALS.json` file to the correct location:
    ```
    mv CREDENTIALS.json ~/.gcloud/sls-android-rtdn.json
    ```

### Create the deployment and connect to RTDN in the PLay Console
The GCP topic that is needed to input in the Play Console to enable RTDN is created outside of the sls deployment (CFN stack in AWS lingo). When a project is created in GCP there's a service account created automatically of the form `INTERNAL_PROJECT_ID@cloudservices.gserviceaccount.com`. This service account needs to have the appropiate role to be able to create deployments. So you need to set that role before your you can do any deployments:
```
gcloud projects add-iam-policy-binding sls-android-rtdn --member serviceAccount:INTERNAL_PROJECT_ID@cloudservices.gserviceaccount.com --role roles/owner
```
1. Create the deployment
    To actually create the deployment run the following command:
    ```
    gcloud deployment-manager deployments create play-realtime-developer-notifications --config deployment.yml
    ```

2. After the topic has been created you need to configure your Play App to send the RTDN to that topic. Follow this guide: https://developer.android.com/google/play/billing/realtime_developer_notifications#enable_real-time_developer_notifications_for_your_app. Make sure that you send a test message and get a confirmation.

3. Take the name(`projects/sls-android-rtdn/topics/status_updates`) of the topic that you created  and plug it into the event definition of the `subscriber` function in the `serverless.yml` file.

4. From this point on you can follow the normal sls workflow to deploy.

## Functions
### `subscriber`
Example implementation just printing the consumed pubsub message.
